# Changelog

## Current Development

## v0.4.0
- Remove impls of `embedded-hal` `0.2` traits which are not in `1.0`
  - Implement the removed features directly on the types
- Use `DelayNs` trait from `embedded-hal` `1.0` for delays

## v0.3.0
- Redesign `Clock` trait to resemble `std::time::Instant` time handling
- Make `Clock` trait methods infallible
- Add `Instant` trait as associated trait to `Clock` trait
- Add a few types which implement the `Instant` trait which can be used by library users to implement a `Clock`

## v0.2.0
- All types implement `Copy` and `Clone`

## v0.1.0
- Initial version
- `Clock` trait, `Timer` and `Delay` types
- Types implement `embedded-hal 0.2` traits where possible
