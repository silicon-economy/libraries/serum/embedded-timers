{ nightly ? false }:
let
  nixpkgsBase = import (fetchTarball {
    name = "nixpkgs-stable-24.05";
    url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/24.05.tar.gz";
    sha256 = "1lr1h35prqkd1mkmzriwlpvxcb34kmhc9dnr48gkm8hh089hifmx";
  });

  # Oxalica's Rust overlay gives us the rust-bin function which allows us
  # to select a specific Rust toolchain. Furthermore, we can configure
  # additional targets like shown below.
  rustOverlay = import (builtins.fetchTarball {
    name = "rust-overlay-2024-11-19";
    url = "https://github.com/oxalica/rust-overlay/archive/71287228d96e9568e1e70c6bbfa3f992d145947b.tar.gz";
    sha256 = "1sg1myrnrfyv7jn5srinrfinvbz8qzly5mmd3rshz9dl0039lh14";
  });

  nixpkgs = nixpkgsBase {
    overlays = [ rustOverlay ];
  };

  # Choose between a specific Rust channel and 'latest', between stable and nightly
  # For nightly, we use a specific one so we do not download a new compiler every day
  rustChannel = (
    if nightly
    then nixpkgs.rust-bin.nightly."2024-11-19"
    # Stable Version of current nixpkgs
    else nixpkgs.rust-bin.stable."1.82.0"
  ).default;

  # cargo-udeps only runs on nightly so we only need to include it if on nightly
  # this lets us skip downloading another nixpkgs when not required
  optionalCargoUdeps =
    if nightly
    then [ nixpkgs.cargo-udeps ]
    else [ ];
in
with nixpkgs;
mkShell {
  buildInputs = [
    rustChannel
    cargo-deny
    cargo-readme
    gitlab-clippy
    cargo-tarpaulin
    cargo2junit
    nodePackages.cspell
    git
  ] ++ optionalCargoUdeps;

  RUST_BACKTRACE = 1;
}

